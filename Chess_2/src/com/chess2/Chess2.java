package com.chess2;

import com.chess2.engine.board.Board;

public class Chess2 {

	
	public static void main(String[] args) {
		
		Board b = Board.createStandardBoard();
		b.printBoard();
		
	}
	
}
