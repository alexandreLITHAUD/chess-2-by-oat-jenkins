package com.chess2.engine.pieces;

import java.util.Collection;

import com.chess2.engine.board.Board;
import com.chess2.engine.move.Move;
import com.chess2.engine.player.Alliance;

public class King extends Piece{

	public boolean hasBanana = true;
	
	public King(Alliance pieceAlliance,int piecePosition) {
		super(piecePosition, PiecesType.KING_BANANA, pieceAlliance, true);
		// TODO Auto-generated constructor stub
	}
	
	public King(Alliance pieceAlliance, PiecesType pt,int piecePosition) {
		super(piecePosition, pt, pieceAlliance, true);
		// TODO Auto-generated constructor stub
	}
	
	public King(int piecePosition, Alliance pieceAlliance, boolean isFirstMove) {
		super(piecePosition, PiecesType.KING_BANANA, pieceAlliance, isFirstMove);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Collection<Move> getLegalMoves(Board board) {
		// TODO Auto-generated method stub
		return null;
	}

}
