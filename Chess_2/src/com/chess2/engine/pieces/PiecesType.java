package com.chess2.engine.pieces;

public enum PiecesType {

	BEAR("B", 200),
	KING("K", 1000),
	KING_BANANA("KB", 1250),
	QUEEN("Q", 2000),
	ELEPHANT("E", 300),
	FISH("P", 100),
	FISH_QUEEN("PQ", 1500),
	MONKEY("M", 700),
	ROOK("R", 500);
	
	private String pieceName;
	private int pieceValue;
	
	private PiecesType(String piece, int pieceValue) {
		this.pieceName = piece;
		this.pieceValue = pieceValue;
	}
	
	@Override
	public String toString() {
		return this.pieceName;
	}
	
	public int getPieceValue() {
		return this.pieceValue;
	}
	
}
