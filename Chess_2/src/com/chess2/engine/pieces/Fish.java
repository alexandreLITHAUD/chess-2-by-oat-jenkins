package com.chess2.engine.pieces;

import java.util.Collection;

import com.chess2.engine.board.Board;
import com.chess2.engine.move.Move;
import com.chess2.engine.player.Alliance;

public class Fish extends Piece{

	public Fish(Alliance pieceAlliance,int piecePosition) {
		super(piecePosition, PiecesType.FISH, pieceAlliance, true);
		// TODO Auto-generated constructor stub
	}
	
	public Fish(int piecePosition, Alliance pieceAlliance, boolean isFirstMove) {
		super(piecePosition, PiecesType.FISH, pieceAlliance, isFirstMove);
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public Collection<Move> getLegalMoves(Board board) {
		// TODO Auto-generated method stub
		return null;
	}

}
