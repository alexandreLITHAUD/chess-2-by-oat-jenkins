package com.chess2.engine.pieces;

import java.util.Collection;

import com.chess2.engine.board.Board;
import com.chess2.engine.move.Move;
import com.chess2.engine.player.Alliance;

public abstract class Piece {

	protected int piecePosition;
	protected PiecesType pieceType;
	protected Alliance pieceAlliance;
	protected boolean isFirstMove;
	
	public Piece(int piecePosition, PiecesType pieceType, Alliance pieceAlliance, boolean isFirstMove) {
		this.piecePosition = piecePosition;
		this.pieceType = pieceType;
		this.pieceAlliance = pieceAlliance;
		this.isFirstMove = isFirstMove;
	}	
	
	public int getPiecePosition() {
		return piecePosition;
	}

	public PiecesType getPieceType() {
		return pieceType;
	}

	public Alliance getPieceAlliance() {
		return pieceAlliance;
	}

	public boolean isFirstMove() {
		return isFirstMove;
	}
	
	@Override
	public String toString() {
		return this.pieceAlliance.toString()+this.pieceType.toString();
	}
	

	public abstract Collection<Move> getLegalMoves(Board board);
}
