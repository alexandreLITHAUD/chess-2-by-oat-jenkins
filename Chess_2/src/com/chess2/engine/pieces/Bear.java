package com.chess2.engine.pieces;

import java.util.Collection;

import com.chess2.engine.board.Board;
import com.chess2.engine.move.Move;
import com.chess2.engine.player.Alliance;

public class Bear extends Piece{

	public Bear(Alliance pieceAlliance,int piecePosition) {
		super(piecePosition, PiecesType.BEAR, pieceAlliance, true);
		// TODO Auto-generated constructor stub
	}
	
	public Bear(int piecePosition, Alliance pieceAlliance, boolean isFirstMove) {
		super(piecePosition, PiecesType.BEAR, pieceAlliance, isFirstMove);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public Collection<Move> getLegalMoves(Board board) {
		// TODO Auto-generated method stub
		return null;
	}

}
