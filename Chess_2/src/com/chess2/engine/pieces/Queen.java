package com.chess2.engine.pieces;

import java.util.Collection;

import com.chess2.engine.board.Board;
import com.chess2.engine.move.Move;
import com.chess2.engine.player.Alliance;

public class Queen extends Piece{

	public Queen(Alliance pieceAlliance,int piecePosition) {
		super(piecePosition, PiecesType.QUEEN, pieceAlliance, true);
		// TODO Auto-generated constructor stub
	}
	
	public Queen(int piecePosition, Alliance pieceAlliance, boolean isFirstMove) {
		super(piecePosition, PiecesType.QUEEN, pieceAlliance, isFirstMove);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Collection<Move> getLegalMoves(Board board) {
		// TODO Auto-generated method stub
		return null;
	}

}
