package com.chess2.engine.board;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.chess2.engine.pieces.Bear;
import com.chess2.engine.pieces.Elephant;
import com.chess2.engine.pieces.Fish;
import com.chess2.engine.pieces.King;
import com.chess2.engine.pieces.Monkey;
import com.chess2.engine.pieces.Piece;
import com.chess2.engine.pieces.Queen;
import com.chess2.engine.pieces.Rook;
import com.chess2.engine.player.Alliance;
import com.chess2.engine.player.BlackPlayer;
import com.chess2.engine.player.Player;
import com.chess2.engine.player.WhitePlayer;
import com.chess2.engine.utils.BoardUtils;

public class Board {
	
	private final List<Tile> gameBoard;

	//private final Collection<Piece> whitePieces;
	//private final Collection<Piece> blackPieces;

//	private final WhitePlayer whitePlayer;
//	private final BlackPlayer blackPlayer;
//	private final Player currentPlayer;
	
	public Board(Builder builder) {
		this.gameBoard = createGameBoard(builder);
		
		//this.whitePieces = calculateActivePieces(this.gameBoard, Alliance.WHITE);
		//this.blackPieces = calculateActivePieces(this.gameBoard, Alliance.BLACK);

		//final Collection<Move> whiteStandardLegalMoves = calculateLegalMoves(this.whitePieces);
		//final Collection<Move> blackStandardLegalMove = calculateLegalMoves(this.blackPieces);

		//this.whitePlayer = new WhitePlayer(this, whiteStandardLegalMoves, blackStandardLegalMove);
		//this.blackPlayer = new BlackPlayer(this, whiteStandardLegalMoves, blackStandardLegalMove);
		//this.currentPlayer = builder.nextMoveMaker.choosePlayer(this.whitePlayer, this.blackPlayer);
	}
	
//	public Collection<Piece> getBlackPieces() {
//		return this.blackPieces;
//	}
//
//	public Collection<Piece> getWhitePieces() {
//		return this.whitePieces;
//	}
	
//	public Player whitePlayer() {
//		return this.whitePlayer;
//	}
//
//	public Player blackplayer() {
//		return this.blackPlayer;
//	}

//	public Player currentPlayer() {
//		return this.currentPlayer;
//	}

	public Tile getTile(final int tileCoordinate) {
		return gameBoard.get(tileCoordinate);
	}
	
	private static List<Tile> createGameBoard(final Builder builder) {

		final Tile[] tiles = new Tile[BoardUtils.NUM_TILES];
		for (int i = 0; i < BoardUtils.NUM_TILES; i++) {
			tiles[i] = Tile.createTile(i, builder.boardConfig.get(i));
		}
		
		return Arrays.asList(tiles);

	}
	
	public void printBoard() {
		
		for(int i = 0; i<BoardUtils.COMMON_NUM_TILES;i++) {
			
			if(i%BoardUtils.NUM_TILES_PER_ROW == 0 && (BoardUtils.FOURTH_RANK[i])) {
				System.out.print("| " + gameBoard.get(64).toString() + " |  ");
			}
			else if(i%BoardUtils.NUM_TILES_PER_ROW == 0 && (BoardUtils.FIFTH_RANK[i])) {
				System.out.print("| " + gameBoard.get(65).toString() + " |  ");
			}
			else if(i%BoardUtils.NUM_TILES_PER_ROW == 0) {
				System.out.print("    ");
			}
			
			System.out.print("| " + gameBoard.get(i).toString() + " |");
			
			if(i%BoardUtils.NUM_TILES_PER_ROW == BoardUtils.NUM_TILES_PER_ROW-1 && (BoardUtils.FOURTH_RANK[i])) {
				System.out.print("  | " + gameBoard.get(66).toString() + " |");
				System.out.print("\n");
			}
			else if(i%BoardUtils.NUM_TILES_PER_ROW == BoardUtils.NUM_TILES_PER_ROW-1 && (BoardUtils.FIFTH_RANK[i])) {
				System.out.print("  | " + gameBoard.get(67).toString() + " |");
				System.out.print("\n");
			}
			else if(i%BoardUtils.NUM_TILES_PER_ROW == BoardUtils.NUM_TILES_PER_ROW-1) {
				System.out.print("\n");
			}
			
		}
		
		
	}
	
	public static Board createStandardBoard() {

		final Builder builder = new Builder();

		// black layout
		builder.setPiece(new Rook(Alliance.BLACK, 0));
		builder.setPiece(new Monkey(Alliance.BLACK, 1));
		builder.setPiece(new Fish(Alliance.BLACK, 2));
		builder.setPiece(new Queen(Alliance.BLACK, 3));
		builder.setPiece(new King(Alliance.BLACK, 4));
		builder.setPiece(new Fish(Alliance.BLACK, 5));
		builder.setPiece(new Monkey(Alliance.BLACK, 6));
		builder.setPiece(new Rook(Alliance.BLACK, 7));
		builder.setPiece(new Fish(Alliance.BLACK, 8));
		builder.setPiece(new Fish(Alliance.BLACK, 9));
		builder.setPiece(new Elephant(Alliance.BLACK, 10));
		builder.setPiece(new Fish(Alliance.BLACK, 11));
		builder.setPiece(new Fish(Alliance.BLACK, 12));
		builder.setPiece(new Elephant(Alliance.BLACK, 13));
		builder.setPiece(new Fish(Alliance.BLACK, 14));
		builder.setPiece(new Fish(Alliance.BLACK, 15));

		// White Layout

		builder.setPiece(new Rook(Alliance.WHITE, 56));
		builder.setPiece(new Monkey(Alliance.WHITE, 57));
		builder.setPiece(new Fish(Alliance.WHITE, 58));
		builder.setPiece(new Queen(Alliance.WHITE, 59));
		builder.setPiece(new King(Alliance.WHITE, 60));
		builder.setPiece(new Fish(Alliance.WHITE, 61));
		builder.setPiece(new Monkey(Alliance.WHITE, 62));
		builder.setPiece(new Rook(Alliance.WHITE, 63));
		builder.setPiece(new Fish(Alliance.WHITE, 48));
		builder.setPiece(new Fish(Alliance.WHITE, 49));
		builder.setPiece(new Elephant(Alliance.WHITE, 50));
		builder.setPiece(new Fish(Alliance.WHITE, 51));
		builder.setPiece(new Fish(Alliance.WHITE, 52));
		builder.setPiece(new Elephant(Alliance.WHITE, 53));
		builder.setPiece(new Fish(Alliance.WHITE, 54));
		builder.setPiece(new Fish(Alliance.WHITE, 55));
		
		builder.setPiece(new Bear(Alliance.BOTH,28));

		builder.setMoveMaker(Alliance.WHITE);
		
		return builder.build();
	}
	
	public static class Builder {

		private Map<Integer, Piece> boardConfig;
		private Alliance nextMoveMaker;

		public Builder() {
			this.boardConfig = new HashMap<>(); // important
		}

		public Builder setPiece(final Piece piece) {
			this.boardConfig.put(piece.getPiecePosition(), piece);
			return this;
		}

		public Builder setMoveMaker(final Alliance nextMoveMaker) {
			this.nextMoveMaker = nextMoveMaker;
			return this;
		}

		public Board build() {
			return new Board(this);
		}

	}
}
