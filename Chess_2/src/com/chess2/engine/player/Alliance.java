package com.chess2.engine.player;

import com.chess2.engine.utils.BoardUtils;

public enum Alliance {
	BLACK {
		@Override
		public boolean isWhite() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isBlack() {
			// TODO Auto-generated method stub
			return true;
		}

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "B";
		}

		@Override
		public boolean isPawnPromotionSquare(int position) {
			// TODO Auto-generated method stub
			return BoardUtils.FIRST_RANK[position];
		}

		@Override
		public Player choosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer) {
			// TODO Auto-generated method stub
			return whitePlayer;
		}

		@Override
		public int getDirection() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public int getOppositeDirection() {
			// TODO Auto-generated method stub
			return -1;
		}
	},
	WHITE {
		@Override
		public boolean isWhite() {
			// TODO Auto-generated method stub
			return true;
		}

		@Override
		public boolean isBlack() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "W";
		}

		@Override
		public boolean isPawnPromotionSquare(int position) {
			// TODO Auto-generated method stub
			return BoardUtils.EIGHTH_RANK[position];
		}

		@Override
		public Player choosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer) {
			// TODO Auto-generated method stub
			return blackPlayer;
		}

		@Override
		public int getDirection() {
			// TODO Auto-generated method stub
			return -1;
		}

		@Override
		public int getOppositeDirection() {
			// TODO Auto-generated method stub
			return 1;
		}
	},
	
	BOTH { // BOTH IS GOOD
		@Override
		public boolean isWhite() {
			// TODO Auto-generated method stub
			return true;
		}

		@Override
		public boolean isBlack() {
			// TODO Auto-generated method stub
			return true;
		}

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "#";
		}

		@Override
		public boolean isPawnPromotionSquare(int position) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public Player choosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getDirection() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int getOppositeDirection() {
			// TODO Auto-generated method stub
			return 0;
		}
	};
	
	public abstract boolean isWhite();
	public abstract boolean isBlack();
	@Override
	public abstract String toString();
	
	public abstract boolean isPawnPromotionSquare(int position);

	public abstract Player choosePlayer(final WhitePlayer whitePlayer, final BlackPlayer blackPlayer);
	
	public abstract int getDirection();
	
	public abstract int getOppositeDirection();
}
